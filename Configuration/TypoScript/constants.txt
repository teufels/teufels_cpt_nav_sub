########################################
## INCLUDES FOR STAGING && PRODUCTION ##
########################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:teufels_cpt_nav_sub/Configuration/TypoScript/Constants/Production" extensions="txt">

##############################
## OVERRIDE FOR DEVELOPMENT ##
##############################
[globalString = ENV:HTTP_HOST=development.*]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:teufels_cpt_nav_sub/Configuration/TypoScript/Constants/Development" extensions="txt">
[global]